public class BreakDemo {
    // there are two ways here, so the string appears two times
    public static void main (String[] args){
        int[] arrayOfInts =
                {1,2,3,4,5,6};
        int searchfor =3;
        int i;
        boolean found = false;
        for (i=0;i<arrayOfInts.length; i++) {
            if (arrayOfInts[i] == searchfor) {
                found = true;
                break;
            }
        }
        if (found) {
            System.out.println("Found " + searchfor + " at index " + i);}
        else {
            System.out.println(searchfor + " Is not in the array");
        }
        // now everything same, but with lable
        found = false;
        search:
        for (i=0;i<arrayOfInts.length; i++) {
            if (arrayOfInts[i] == searchfor) {
                found = true;
                break search;
            }
        }
        if (found) {
            System.out.println("Found " + searchfor + " at index " + i);}
        else {
            System.out.println(searchfor + " Is not in the array");
        }
    }
}

